package com.example.cameraapp.util

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Matrix
import android.util.Log
import androidx.camera.core.ImageProxy
import com.example.cameraapp.Constants

class ConvertToBitmap(mContext:Context) {
    private val yuvToRgbConverter = YuvToRgbConverter(mContext)
    private lateinit var bitmapBuffer: Bitmap
    private lateinit var rotationMatrix: Matrix
    @SuppressLint("UnsafeExperimentalUsageError", "UnsafeOptInUsageError")
    public fun toBitmap(imageProxy: ImageProxy): Bitmap? {
        val image = imageProxy.image ?: return null
        // Initialise Buffer
        if (!::bitmapBuffer.isInitialized) {
            // The image rotation and RGB image buffer are initialized only once
            Log.d(Constants.TAG, "Initalise toBitmap()")
            rotationMatrix = Matrix()
            rotationMatrix.postRotate(imageProxy.imageInfo.rotationDegrees.toFloat())
            bitmapBuffer = Bitmap.createBitmap(
                imageProxy.width, imageProxy.height, Bitmap.Config.ARGB_8888
            )
        }
        // Pass image to an image analyser
        yuvToRgbConverter.yuvToRgb(image, bitmapBuffer)
        // Create the Bitmap in the correct orientation
        return Bitmap.createBitmap(
            bitmapBuffer,
            0,
            0,
            bitmapBuffer.width,
            bitmapBuffer.height,
            rotationMatrix,
            false
        )
    }
}