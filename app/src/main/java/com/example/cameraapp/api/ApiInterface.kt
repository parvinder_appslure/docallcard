package com.example.cameraapp.api
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {
    @POST("scan_card_data")
     fun postCard(@Body post: PostCard):Call<ResponseCard>
     @POST("host_machine_login")
     fun login(@Body post:PostLogin):Call<ResponseLogin>
}